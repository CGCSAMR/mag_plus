//
// Created by gedoix on 10-01-19.
//

#ifndef SUCHAI_FLIGHT_SOFTWARE_TASKTEST_H
#define SUCHAI_FLIGHT_SOFTWARE_TASKTEST_H

#include "suchai/config.h"
#include "globals.h"

#include "osDelay.h"

#include "repoCommand.h"

void taskTest(void* param);

#endif //SUCHAI_FLIGHT_SOFTWARE_TASKTEST_H

/**
 * @file  cmdRPI.h
 * @author Matias Vidal Valladares - matias.vidal@ing.uchile.cl
 * @date 2021
 * @copyright GNU GPL v3
 *
 * This header has definitions of commands related to the Raspberry Pi driver features.
 */

#ifndef CMD_RPI_H
#define CMD_RPI_H

#include "suchai/config.h"

#include "suchai/repoCommand.h"
#include "suchai/repoData.h"

/**
 * Register the Raspberry Pi commands
 */
void cmd_rpi_init(void);

/**
 * Checks wether the camera is enabled or not and saves it as a status variable.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_check_camera(char *fmt, char *params, int nparams);

/**
 * Checks wether the I2C is enabled or not and saves it as a status variable.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_check_i2c(char *fmt, char *params, int nparams);

/**
 * Checks wether the serial is enabled or not and saves it as a status variable.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_check_serial(char *fmt, char *params, int nparams);

/**
 * Checks wether the spi is enabled or not and saves it as a status variable.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_check_spi(char *fmt, char *params, int nparams);

/**
 * Checks the status variables of the Raspberry Pi.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_check_status(char *fmt, char *params, int nparams);

/**
 * Disables the Raspberry Pi's camera.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_disable_camera(char *fmt, char *params, int nparams);

/**
 * Disables the bluetooth of the Raspberry Pi to save power.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_disable_bluetooth(char *fmt, char *params, int nparams);

/**
 * Disables the Raspberry Pi's HDMI.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_disable_hdmi(char *fmt, char *params, int nparams);

/* Disables the Raspberry Pi's I2C.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_disable_i2c(char *fmt, char *params, int nparams);


/* Disables the Raspberry Pi's LED.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_disable_led(char *fmt, char *params, int nparams);

/* Disables the Raspberry Pi's SPI.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_disable_spi(char *fmt, char *params, int nparams);

/*
 * Disables the Raspberry Pi's wifi.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_disable_wifi(char *fmt, char *params, int nparams);

/**
 * Enables the Raspberry Pi's camera.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_enable_camera(char *fmt, char *params, int nparams);

/*
 * Re-enables the Raspberry Pi's bluetooth.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_enable_bluetooth(char *fmt, char *params, int nparams);

/*
 * Re-enables the Raspberry Pi's HDMI.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_enable_hdmi(char *fmt, char *params, int nparams);

/* Enables the Raspberry Pi's I2C.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_enable_i2c(char *fmt, char *params, int nparams);

/*
 * Re-enables the Raspberry Pi's LED.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_enable_led(char *fmt, char *params, int nparams);

/* Enables the Raspberry Pi's SPI.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_enable_spi(char *fmt, char *params, int nparams);

/*
 * Re-enables the Raspberry Pi's wifi.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_enable_wifi(char *fmt, char *params, int nparams);

/*
 * Records a video and saves it using the date and time as the name.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_record(char *fmt, char *params, int nparams);

/*
 * Execute a system call and prints its output.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string <node, rpi_sys_call>.
 *                    Ex: "2 df -h"
 * @param nparams Int. Number of parameters 2
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_system(char *fmt, char *params, int nparams);

/*
 * Takes a picture and saves it using the date and time as the name.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_take_picture(char *fmt, char *params, int nparams);

/*
 * Prints all the avaliable commands for this payload.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int rpi_help(char *fmt, char *params, int nparams);

#endif /* CMD_RPI_H */

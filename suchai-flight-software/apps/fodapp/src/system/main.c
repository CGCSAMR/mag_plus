/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *      Copyright 2021, Carlos Gonzalez Cortes, carlgonz@uchile.cl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "suchai/mainFS.h"
#include "suchai/taskInit.h"
#include "suchai/osThread.h"
#include "suchai/log_utils.h"
#include "app/system/taskHousekeeping.h"
#include "app/system/taskFOD.h"
#include "app/system/cmdFOD.h"
#include "app/system/cmdRPI.h"

static char *tag = "app_main";

/**
 * App specific initialization routines
 * This function is called by taskInit
 *
 * @param params taskInit params
 */
void initAppHook(void *params)
{
    /** Include fod app commands */
    cmd_fod_init();

    /** Include RPI commands */
    cmd_rpi_init();

    /** Initialize custom CSP interfaces */
    csp_add_zmq_iface(SCH_COMM_NODE);

    /** Init app tasks */
    int t_ok = osCreateTask(taskHousekeeping, "housekeeping", 1024, NULL, 2, NULL);
    if(t_ok != 0) LOGE("fod-app", "Task housekeeping not created!");
    int tfod_ok = osCreateTask(taskFOD, "hello", 1024, NULL, 2, NULL);
    if(tfod_ok != 0) LOGE("fod-app", "Task FOD not created!");
}

int main(void)
{
    /** Call framework main, shouldn't return */
    suchai_main();
}

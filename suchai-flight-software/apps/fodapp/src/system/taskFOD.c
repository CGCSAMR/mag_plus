#include "app/system/taskFOD.h"

void taskFOD(void *params){
   cmd_t *cmd = cmd_build_from_str("fod_pwr 1");
   cmd_send(cmd);
    while(1){
        osDelay(10000);
        LOGI("taskFOD", "Sending beacon!");
        cmd_t *cmd = cmd_build_from_str("fod_send_beacon");
        cmd_send(cmd);
    }
}

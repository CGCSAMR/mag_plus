from datetime import datetime
from random import random

dt = datetime.utcnow()
node = 1
fe_index = 0
date = int(dt.day*10000 + dt.month*100 + dt.year - 2000)
time = int(dt.hour*1000000 + dt.minute*10000 + dt.second*100 + dt.microsecond*0.0001)
latitude = -33.457827123
longitude = -70.661678605
altitude_km = 505.5
speed_mps = 1.7
fe_mag_x = random()
fe_mag_y = random()
fe_mag_z = random()
num_sats = 15
print("{:d} {:d} {:d} {:d} {:.9f} {:.9f} {:f} {:f} {:f} {:f} {:f} {:d}".format(node,
                                                                               fe_index,
                                                                               date,
                                                                               time,
                                                                               latitude,
                                                                               longitude,
                                                                               altitude_km,
                                                                               speed_mps,
                                                                               fe_mag_x,
                                                                               fe_mag_y,
                                                                               fe_mag_z,
                                                                               num_sats))

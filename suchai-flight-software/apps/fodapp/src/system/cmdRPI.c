#include "app/system/cmdRPI.h"

static const char* tag = "cmdRPI";
uint32_t rpi_first_cmd_idx = 0;
uint32_t rpi_last_cmd_idx = 0;

void cmd_rpi_init(void) {
    rpi_first_cmd_idx = cmd_add("rpi_check_camera", rpi_check_camera, "", 0) - 1;
    cmd_add("rpi_check_i2c", rpi_check_i2c, "", 0);
    cmd_add("rpi_check_serial", rpi_check_serial, "", 0);
    cmd_add("rpi_check_spi", rpi_check_spi, "", 0);
    cmd_add("rpi_check_status", rpi_check_status, "", 0);
    cmd_add("rpi_disable_camera", rpi_disable_camera, "", 0);
    cmd_add("rpi_disable_bluetooth", rpi_disable_bluetooth, "", 0);
    cmd_add("rpi_disable_hdmi", rpi_disable_hdmi, "", 0);
    cmd_add("rpi_disable_i2c", rpi_disable_i2c, "", 0);
    cmd_add("rpi_disable_led", rpi_disable_led, "", 0);
    cmd_add("rpi_disable_spi", rpi_disable_spi, "", 0);
    cmd_add("rpi_disable_wifi", rpi_disable_wifi, "", 0);
    cmd_add("rpi_enable_camera", rpi_enable_camera, "", 0);
    cmd_add("rpi_enable_bluetooth", rpi_enable_bluetooth, "", 0);
    cmd_add("rpi_enable_hdmi", rpi_enable_hdmi, "", 0);
    cmd_add("rpi_enable_i2c", rpi_enable_i2c, "", 0);
    cmd_add("rpi_enable_led", rpi_enable_led, "", 0);
    cmd_add("rpi_enable_spi", rpi_enable_spi, "", 0);
    cmd_add("rpi_enable_wifi", rpi_enable_wifi, "", 0);
    cmd_add("rpi_record", rpi_record, "", 0);
    cmd_add("rpi_system", rpi_system, "%u %s", 2);
    cmd_add("rpi_take_picture", rpi_take_picture, "", 0);
    rpi_last_cmd_idx = cmd_add("rpi_help", rpi_help, "", 0);
}

int rpi_check_camera(char *fmt, char *params, int nparams) {
    LOGI(tag, "Checking the camera's status...");
    FILE *rpi_f;
    char rpi_output[4];
    uint32_t status;
    #ifdef RPI
    rpi_f = popen("sudo raspi-config nonint get_camera", "r");
    fgets(rpi_output, sizeof(rpi_output), rpi_f);
    status = (uint32_t)rpi_output[0] - 48;
    LOGI(tag, "Status (0 is enabled): %u", status);
    dat_set_system_var(dat_rpi_camera, status);
    pclose(rpi_f);
    #endif
    LOGI(tag, "Done");
    return CMD_OK;
}

int rpi_check_i2c(char *fmt, char *params, int nparams) {
    LOGI(tag, "Checking the I2C status...");
    FILE *rpi_f;
    char rpi_output[4];
    uint32_t status;
    #ifdef RPI
    rpi_f = popen("sudo raspi-config nonint get_i2c", "r");
    fgets(rpi_output, sizeof(rpi_output), rpi_f);
    status = (uint32_t)rpi_output[0] - 48;
    LOGI(tag, "Status (0 is enabled): %u", status);
    dat_set_system_var(dat_rpi_i2c, status);
    pclose(rpi_f);
    #endif
    LOGI(tag, "Done");
    return CMD_OK;
}

int rpi_check_serial(char *fmt, char *params, int nparams) {
    LOGI(tag, "Checking the Serial status...");
    FILE *rpi_f;
    char rpi_output[4];
    uint32_t status;
    #ifdef RPI
    rpi_f = popen("sudo raspi-config nonint get_serial", "r");
    fgets(rpi_output, sizeof(rpi_output), rpi_f);
    status = (uint32_t)rpi_output[0] - 48;
    LOGI(tag, "Status (0 is enabled): %u", status);
    dat_set_system_var(dat_rpi_serial, status);
    pclose(rpi_f);
    #endif
    LOGI(tag, "Done");
    return CMD_OK;
}

int rpi_check_spi(char *fmt, char *params, int nparams) {
    LOGI(tag, "Checking the SPI status...");
    FILE *rpi_f;
    char rpi_output[4];
    uint32_t status;
    #ifdef RPI
    rpi_f = popen("sudo raspi-config nonint get_spi", "r");
    fgets(rpi_output, sizeof(rpi_output), rpi_f);
    status = (uint32_t)rpi_output[0] - 48;
    LOGI(tag, "Status (0 is enabled): %u", status);
    dat_set_system_var(dat_rpi_spi, status);
    pclose(rpi_f);
    #endif
    LOGI(tag, "Done");
    return CMD_OK;
}

int rpi_check_status(char *fmt, char *params, int nparams) {
    LOGI(tag, "Checking the RPI's status...");
    uint32_t camera_status = dat_get_system_var(dat_rpi_camera);
    uint32_t i2c_status = dat_get_system_var(dat_rpi_i2c);
    uint32_t serial_status = dat_get_system_var(dat_rpi_serial);
    uint32_t spi_status = dat_get_system_var(dat_rpi_spi);
    LOGI(tag, "Raspberry pi status:");
    printf("Camera: %u\nI2C: %u\nSerial: %u\nSPI: %u\n", camera_status,
		                                         i2c_status,
						         serial_status,
						         spi_status);
    return CMD_OK;
}

int rpi_disable_camera(char *fmt, char *params, int nparams) {
    LOGI(tag, "Disabling camera");
    #ifdef RPI
    system("sudo raspi-config nonint do_camera 1");
    LOGI(tag, "Done")
    #endif
    return CMD_OK;
}

int rpi_disable_bluetooth(char *fmt, char *params, int nparams) {
    LOGI(tag, "Disabling bluetooth");
    #ifdef RPI
    system("sudo rfkill block bluetooth");
    LOGI(tag, "Done")
    #endif
    return CMD_OK;
}

int rpi_disable_hdmi(char *fmt, char *params, int nparams) {
    LOGI(tag, "Disabling HDMI");
    #ifdef RPI
    system("/usr/bin/tvservice -o");
    LOGI(tag, "Done")
    #endif
    return CMD_OK;
}

int rpi_disable_i2c(char *fmt, char *params, int nparams) {
    LOGI(tag, "Disabling I2C");
    #ifdef RPI
    system("sudo raspi-config nonint do_i2c 1");
    LOGI(tag, "Done")
    #endif
    return CMD_OK;
}

int rpi_disable_led(char *fmt, char *params, int nparams) {
    LOGI(tag, "Disabling LED");
    #ifdef RPI
    system("echo none | sudo tee /sys/class/leds/led0/trigger");
    LOGI(tag, "Done")
    #endif
    return CMD_OK;
}

int rpi_disable_spi(char *fmt, char *params, int nparams) {
    LOGI(tag, "Disabling SPI");
    #ifdef RPI
    system("sudo raspi-config nonint do_spi 1");
    LOGI(tag, "Done")
    #endif
    return CMD_OK;
}

int rpi_disable_wifi(char *fmt, char *params, int nparams) {
    LOGI(tag, "Disabling wifi");
    #ifdef RPI
    system("sudo rfkill block wifi");
    LOGI(tag, "Done")
    #endif
    return CMD_OK;
}

int rpi_enable_camera(char *fmt, char *params, int nparams) {
    LOGI(tag, "Enabling camera");
    #ifdef RPI
    system("sudo raspi-config nonint do_camera 0");
    LOGI(tag, "Done")
    #endif
    return CMD_OK;
}

int rpi_enable_bluetooth(char *fmt, char *params, int nparams) {
    LOGI(tag, "Enabling bluetooth");
    #ifdef RPI
    system("sudo rfkill unblock bluetooth");
    LOGI(tag, "Done")
    #endif
    return CMD_OK;
}

int rpi_enable_hdmi(char *fmt, char *params, int nparams) {
    LOGI(tag, "Enabling HDMI");
    #ifdef RPI
    system("/usr/bin/tvservice -p");
    LOGI(tag, "Done")
    #endif
    return CMD_OK;
}

int rpi_enable_i2c(char *fmt, char *params, int nparams) {
    LOGI(tag, "Enabling I2C");
    #ifdef RPI
    system("sudo raspi-config nonint do_i2c 0");
    LOGI(tag, "Done")
    #endif
    return CMD_OK;
}

int rpi_enable_led(char *fmt, char *params, int nparams) {
    LOGI(tag, "Enabling LED");
    #ifdef RPI
    system("echo 1 | sudo tee /sys/class/leds/led0/trigger");
    LOGI(tag, "Done")
    #endif
    return CMD_OK;
}

int rpi_enable_spi(char *fmt, char *params, int nparams) {
    LOGI(tag, "Enabling SPI");
    #ifdef RPI
    system("sudo raspi-config nonint do_spi 0");
    LOGI(tag, "Done")
    #endif
    return CMD_OK;
}

int rpi_enable_wifi(char *fmt, char *params, int nparams) {
    LOGI(tag, "Enabling wifi");
    #ifdef RPI
    system("sudo rfkill unblock wifi");
    LOGI(tag, "Done")
    #endif
    return CMD_OK;
}

int rpi_record(char *fmt, char *params, int nparams) {
    LOGI(tag, "Recording a video...");
    #ifdef RPI
    char buffer[45];
    uint32_t dat_time = dat_get_time();
    time_t t = time(NULL);
    struct tm local = *localtime(&t);
    snprintf(buffer, sizeof(buffer), "raspivid -o %d_%d_%d_%u.h264 -t 10000", local.tm_year + 1900,
		                                                              local.tm_mon + 1,
					                                      local.tm_mday,
							                      dat_time);
    LOGI(tag, "Executing command %s", buffer);
    system(buffer);
    #endif
    LOGI(tag, "Done");
    return CMD_OK;
}

int rpi_system(char *fmt, char *params, int nparams) {
    FILE *rpi_f;
    char rpi_sys_call[2048];
    char rpi_output[2048];
    uint32_t node;
    if (params == NULL) {
	LOGE(tag, "NULL params!");
	return CMD_SYNTAX_ERROR;
    }
    if (sscanf(params, fmt, &node, &rpi_sys_call) == nparams) {
	LOGI(tag, "Executing: %s", rpi_sys_call);
	rpi_f = popen(rpi_sys_call, "r");
	LOGI(tag, "Output:");
	while (fgets(rpi_output, sizeof(rpi_output), rpi_f) != NULL) {
	    printf(rpi_output);
	}
	LOGI(tag, "Sending output to node %u", node);
	com_send_debug(node, rpi_output, sizeof(rpi_output));
	pclose(rpi_f);
    }
    else {
	LOGE(tag, "Invalid params!");
	return CMD_SYNTAX_ERROR;
    }
    return CMD_OK;
}

int rpi_take_picture(char *fmt, char *params, int nparams) {
    LOGI(tag, "Taking picture...");
    #ifdef RPI
    char buffer[40];
    uint32_t dat_time = dat_get_time();
    time_t t = time(NULL);
    struct tm local = *localtime(&t);
    snprintf(buffer, sizeof(buffer), "raspistill -o %d_%d_%d_%u.jpg", local.tm_year + 1900,
		                                                      local.tm_mon + 1,
								      local.tm_mday,
								      dat_time);
    LOGI(tag, "Executing command %s", buffer);
    system(buffer);
    #endif
    LOGI(tag, "Done");
    return CMD_OK;
}

int rpi_help(char *fmt, char *params, int nparams) {
    const char *rpi_cmd_name;
    const char *rpi_cmd_fmt;
    int i;
    LOGI(tag, "Printing commands from %u to %u:", rpi_first_cmd_idx, rpi_last_cmd_idx-1);
    printf("%5s %s %25s\r\n", "Index", "Name", "Params");
    for(i=rpi_first_cmd_idx; i<rpi_last_cmd_idx; i++) {
	rpi_cmd_name = cmd_get_name(i);
        int printed = printf("%5d %s", i, rpi_cmd_name);
	rpi_cmd_fmt = cmd_get_fmt((char *) rpi_cmd_name);
        if (*rpi_cmd_fmt != '\0')
            printf(" %s\r\n", rpi_cmd_fmt);
        else
            printf("\r\n");
    }
    return CMD_OK;
}

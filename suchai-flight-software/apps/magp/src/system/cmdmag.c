#include "app/system/cmdmag.h"

void cmd_magp_init()
{
	cmd_add("magp_hello", magp_hello, "", 0);
	cmd_add("counter_up", app_up_counter, "", 0);
	cmd_add("counter_down", app_down_counter, "", 0);
	cmd_add("get_data", app_get_data, "", 0);
}

int magp_hello(char *fmt, char *params, int nparams)
{
	LOGI("cmdMagp", "Hello from Mag command!");
	return CMD_OK;
}

int app_up_counter(char *fmt, char *params, int nparams)
{
	int counter = dat_get_system_var(dat_app_cmd_counter);
	dat_set_system_var(dat_app_cmd_counter, counter+1);
}

int app_down_counter(char *fmt, char *params, int nparams)
{
	int counter = dat_get_system_var(dat_app_cmd_counter);
	dat_set_system_var(dat_app_cmd_counter, counter-1);
}


int app_get_data(char *fmt, char *params, int nparams)
{
	app_data_t data;
	data.index = dat_get_system_var(dat_drp_idx_app);
	data.timestamp = dat_get_time();
	data.data = (int32_t)random();
	int rc = dat_add_payload_sample(&data, app_sensor);
	return rc != -1? CMD_OK : CMD_ERROR;
}

import RM3100 as mag
import MCP9808 as temp
import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)

#GPIO 5 MAG+

GPIO.setup(5, GPIO.OUT)
GPIO.output(5, GPIO.LOW)
GPIO.output(5, GPIO.HIGH)

mag1 = mag.launch(0x20)
#mag.measure_1(mag1)

mag2 = mag.launch(0x21)
#mag.measure_1(mag2)

#mag.measure_2(mag1, mag2)

tmp1 = temp.launch(0x18)
#temp.measure_1(tmp1)

tmp2 = temp.launch(0x19)
#temp.measure_1(tmp2)


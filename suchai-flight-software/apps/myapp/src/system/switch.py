import RM3100 as mag
import MCP9808 as temp
import RPi.GPIO as GPIO
import time
import sys
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
#GPIO 5 MAG+

GPIO.setup(5, GPIO.OUT)
GPIO.output(5, GPIO.LOW)
GPIO.output(5, GPIO.HIGH)

mag1 = mag.launch(0x20)
#mag.measure_1(mag1)

mag2 = mag.launch(0x21)
#mag.measure_1(mag2)

#mag.measure_2(mag1, mag2)

tmp1 = temp.launch(0x18)
#temp.measure_1(tmp1)

tmp2 = temp.launch(0x19)
#temp.measure_1(tmp2)

#temp.measure_2(tmp1, tmp2)
star = time.time()

open("/home/pi/mag.txt","w").close()
file = open("/home/pi/mag.txt","w")

for i in range(int(sys.argv[1])):
    #print(i)
    counter1, x1, y1, z1, counter2, x2, y2, z2 = mag.measure_2(mag1, mag2)    
    temp1, temp2 = temp.measure_2(tmp1, tmp2)
    #print("{:d} {:d} {:d} {:d} {:d} {:d} {:d} {:d} {:f} {:f}".format(counter1,
    #                                                                 x1,
    #                                                                 y1,
    #                                                                 z1,
    #                                                                 counter2,
    #                                                                 x2,
    #                                                                 y2,
    #                                                                 z2,
    #                                                                 temp1,
    #                                                                 temp2))
    
    file.write("{:d} {:d} {:d} {:d} {:d} {:d} {:d} {:d} {:f} {:f}\n".format(counter1,
                                                                          x1,
                                                                          y1,
                                                                          z1,
                                                                          counter2,
                                                                          x2,
                                                                          y2,
                                                                          z2,
                                                                          temp1,
                                                                          temp2))
#print(star, time.time())
#print(time.time()-star)
GPIO.output(5, GPIO.LOW)
GPIO.cleanup()
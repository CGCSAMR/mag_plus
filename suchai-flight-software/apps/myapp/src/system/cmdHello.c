#include "app/system/cmdHello.h"

static const char* tag = "cmdHello";
char mag_app_path[65];
char mag_find_cmd[101];

void cmd_hello_init()
{
	cmd_add("say_hello", say_hello, "%u", 1);
	cmd_add("counter_up", app_up_counter, "", 0);
	cmd_add("counter_down", app_down_counter, "", 0);
	//cmd_add("get_data", app_get_data, "", 0);
	cmd_add("mag_get_data", mag_get_data, "%d",1);
	//cmd_add("mag_save_data", mag_get_data, "",0);
	FILE *fp;
        snprintf(mag_find_cmd, sizeof(mag_find_cmd),
        "find /home -type d -name %s 2>/dev/null | grep -v build | grep -v super-mag-plus | tr -d '\n'", APP_NAME);
        fp = popen(mag_find_cmd, "r");
        fgets(mag_app_path, sizeof(mag_app_path), fp);
        pclose(fp);
}

int say_hello(char *fmt, char *params, int nparams) {
    uint32_t node;
    char hello[28] = "I just came to say hello";
    if (params == NULL) {
        LOGE("cmdHello", "NULL params!");
	return CMD_SYNTAX_ERROR;
    }
    if (sscanf(params, fmt, &node) == nparams) {
	LOGI(tag, hello);
	com_send_debug(node, hello, strlen(hello));
    }
    else {
        LOGE(tag, "Invalid params!");
	return CMD_SYNTAX_ERROR;
    }
    return CMD_OK;
}

int app_up_counter(char *fmt, char *params, int nparams)
{
	//int counter = dat_get_system_var(dat_app_cmd_counter);
	//dat_set_system_var(dat_app_cmd_counter, counter+1);
}

int app_down_counter(char *fmt, char *params, int nparams)
{
	//int counter = dat_get_system_var(dat_app_cmd_counter);
	//dat_set_system_var(dat_app_cmd_counter, counter-1);
}
/*
int app_get_data(char *fmt, char *params, int nparams)
{
	/*dat_set_system_var(dat_drp_idx_app, 0);
	dat_set_system_var(dat_drp_ack_app, 0);
	printf("Index: %u\n", dat_get_system_var(dat_drp_idx_app));
	printf("ACK: %u\n", dat_get_system_var(dat_drp_ack_app));
	*
	app_data_t data1;
	data1.index = dat_get_system_var(dat_drp_idx_app);
	data1.timestamp = dat_get_time();
	data1.data = (int32_t)random();
	int rc = dat_add_payload_sample(&data1, app_sensor);
	return rc != -1 ? CMD_OK : CMD_ERROR;
}
*/
	
int mag_get_data(char *fmt, char *params, int nparams)
{
	mag_data_t data1;
	uint32_t mg_msg_len = 128;
	uint32_t time;
	int measurements;
	FILE *mg;
	FILE *py;
	char mag_msg[mg_msg_len];
	char py_msg[mg_msg_len];
	//char path_alt[64] = "/home/pi/suchai-software-template/apps/myapp/src/system/mag.txt";
	//char path[90] = "sudo python3 apps/";
	char path[90];
	sprintf(path, "sudo python3 %s/src/system/switch.py", mag_app_path);
	//strcat(path, APP_NAME);
	//strcat(path, "/src/system/switch.py");
	char number[5];
	int rc;
	int add_rc = 0;
        
	if (params == NULL)
        return CMD_SYNTAX_ERROR;
    
    if (sscanf(params, fmt, &measurements)!= nparams)
        return CMD_SYNTAX_ERROR;
      
    //LOGI("cmd_mag_get_data", "measurements number %d, time %u", measurements, dat_get_time());
    sprintf(number, " %d", measurements);   
    strcat(path, number);
    //LOGI("cmd_mag_get_data", path);
    
    time = dat_get_time();
    //py = popen(path, "r" );
    if (measurements!= 0)
		system(path);
    
    //while (fgets(py_msg, mg_msg_len, py) != NULL)
    //{
	//}
    //pclose(py);
    
    mg = fopen("/home/pi/mag.txt", "r");
	
	if (mg == NULL)
	{
		LOGI("cmd_mag_get_data", "Null error");
		return CMD_ERROR;
    }
    
    while (fgets(mag_msg, mg_msg_len, mg) != NULL)
    {
		sscanf(mag_msg,"%d %d %d %d %d %d %d %d %f %f", &data1.splf,
														&data1.magxf,
														&data1.magyf,
														&data1.magzf,
														&data1.spls,
														&data1.magxs,
														&data1.magys,
														&data1.magzs,
														&data1.tempf,
														&data1.temps);
    
		data1.index = dat_get_system_var(dat_drp_idx_mag);
		//data1.timestamp = dat_get_time();
		data1.timestamp = time;
		rc = dat_add_payload_sample(&data1, mag_sensor);
	        add_rc += rc;
		//LOGI("cmd_mag_get_data", "indx %u tstp %u splf %d magxf %d magyf %d magzf %d ", data1.index,
		//																				data1.timestamp, 
		//																				data1.splf, 
		//																				data1.magxf, 
		//																				data1.magyf, 
		//																				data1.magzf);
		
		if (rc == -1)
			CMD_ERROR;
		
		time = time+1;
	}
	
    fclose(mg);
    char buff[16];
    sprintf(buff, "rc=%d", add_rc);
    com_send_debug(10, buff, strlen(buff));
    return CMD_OK;
}	
int mag_save_data(char *fmt, char *params, int nparams)
{
	mag_data_t data2;
	
	uint32_t mg_msg_len = 128;
	uint32_t time;

	FILE *mg;

	char mag_msg[mg_msg_len];

	int rc;
	int add_rc = 0;
        
    time = dat_get_time();
    
    mg = fopen("/home/pi/mag.txt", "r");
	
	if (mg == NULL)
	{
		LOGI("cmd_mag_get_data", "Null error");
		return CMD_ERROR;
    }
    
    while (fgets(mag_msg, mg_msg_len, mg) != NULL)
    {
		sscanf(mag_msg,"%d %d %d %d %d %d %d %d %f %f", &data2.splf,
														&data2.magxf,
														&data2.magyf,
														&data2.magzf,
														&data2.spls,
														&data2.magxs,
														&data2.magys,
														&data2.magzs,
														&data2.tempf,
														&data2.temps);
    
		data2.index = dat_get_system_var(dat_drp_idx_mag);
		//data1.timestamp = dat_get_time();
		data2.timestamp = time;
		rc = dat_add_payload_sample(&data2, mag_sensor);
		add_rc += rc;
	
		if (rc == -1)
			LOGI("cmd_mag_get_data", "rc error");
			CMD_ERROR;
		
		time = time+1;
    }
    fclose(mg);
    return CMD_OK;
}

#include "suchai/repoCommand.h"

void cmd_hello_init();
int say_hello(char *fmt, char *params, int nparams);
int app_up_counter(char *fmt, char *params, int nparams);
int app_down_counter(char *fmt, char *params, int nparams);
//int app_get_data(char *fmt, char *params, int nparams);
int mag_get_data(char *fmt, char *params, int nparams);
int mag_save_data(char *fmt, char *params, int nparams);

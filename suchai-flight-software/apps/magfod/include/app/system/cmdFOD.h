/**
 * @file  cmdFOD.h
 * @author Matias Vidal Valladares - matias.vidal@ing.uchile.cl
 * @date 2021
 * @copyright GNU GPL v3
 *
 * This header has definitions of commands related to the Femto-satellite Orbital Deployer driver
 * (FOD) features.
 */

#ifndef CMD_FOD_H
#define CMD_FOD_H

#include "suchai/config.h"

#include "suchai/repoCommand.h"
#include "suchai/repoData.h"

#ifdef RPI
#include "bcm2835.h"
#endif

#define FOD_DEPLOY_PIN 17
#define FOD_POWER_PIN 27
#define FOD_STATUS_PIN  4

/**
 * Register the femto-satellite orbital deployer commands
 */
void cmd_fod_init(void);

/**
 * Deploys the femto-satellites by heating a Nichrome wire
 * which cuts the wire that closes the gate.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int deploy_femtosats(char *fmt, char *params, int nparams);

/**
 * Deploys the femto-satellites by heating a Nichrome wire
 * which cuts the wire that closes the gate. The deployment
 * takes a fixed amount of time selected by the user. It will
 * force the deployment even if the release status is 1.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string: <duration>. Ex: "5000"
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int fod_force_deployment(char *fmt, char *params, int nparams);

/**
 * Reads the FOD's configuration.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int fod_get_config(char *fmt, char *params, int nparams);

/* Sends a command to the femto-satellite to retrieve its sensor data.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int get_femtosat_data(char *fmt, char *params, int nparams);

/*
 * Saves the deployment status in a system variable.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int fod_get_status(char *fmt, char *params, int nparams);

/*
 * Prints the current version of the FOD's software.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int fod_get_version(char *fmt, char *params, int nparams);

/*
 * Powers on or off the FOD.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string: <power_on>. Ex: "1"
 * @param nparams Int. Number of parameters 1
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int fod_pwr(char *fmt, char *params, int nparams);

/*
 * Restores the default values.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int fod_reset(char *fmt, char *params, int nparams);

/*
 * Sends a beacon to the femto-satellites.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int fod_send_beacon(char *fmt, char *params, int nparams);

/*
 * Sets the duration in milliseconds for the deployment attempt and saves it
 * in a system variable.
 *
 * @param fmt Str. Parameters format "%u"
 * @param params Str. Parameters as string: <on_time>. Ex: "1500"
 * @param nparams Int. Number of parameters 1
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int set_on_time(char *fmt, char *params, int nparams);


int fod_update_data(char *fmt, char *params, int nparams);

/*
 * Prints all the avaliable commands for this payload.
 *
 * @param fmt Str. Parameters format ""
 * @param params Str. Parameters as string ""
 * @param nparams Int. Number of parameters 0
 * @return  CMD_OK if executed correctly, CMD_ERROR in case of failures, or CMD_SYNTAX_ERROR in case of parameters errors
 */
int fod_help(char *fmt, char *params, int nparams);

#endif /* CMD_FOD_H */

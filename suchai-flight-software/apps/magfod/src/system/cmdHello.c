#include "app/system/cmdHello.h"

void cmd_hello_init()
{
	cmd_add("say_hello", say_hello, "", 0);
	cmd_add("counter_up", app_up_counter, "", 0);
	cmd_add("counter_down", app_down_counter, "", 0);
	//cmd_add("get_data", app_get_data, "", 0);
	cmd_add("mag_get_data", mag_get_data, "%d",1);
	cmd_add("mag_save_data", mag_get_data, "",0);
}

int say_hello(char *fmt, char *params, int nparams)
{
	LOGI("cmdHello", "Hello from command!");
	return CMD_OK;
}

int app_up_counter(char *fmt, char *params, int nparams)
{
	//int counter = dat_get_system_var(dat_app_cmd_counter);
	//dat_set_system_var(dat_app_cmd_counter, counter+1);
}

int app_down_counter(char *fmt, char *params, int nparams)
{
	//int counter = dat_get_system_var(dat_app_cmd_counter);
	//dat_set_system_var(dat_app_cmd_counter, counter-1);
}
/*
int app_get_data(char *fmt, char *params, int nparams)
{
	/*dat_set_system_var(dat_drp_idx_app, 0);
	dat_set_system_var(dat_drp_ack_app, 0);
	printf("Index: %u\n", dat_get_system_var(dat_drp_idx_app));
	printf("ACK: %u\n", dat_get_system_var(dat_drp_ack_app));
	*
	app_data_t data1;
	data1.index = dat_get_system_var(dat_drp_idx_app);
	data1.timestamp = dat_get_time();
	data1.data = (int32_t)random();
	int rc = dat_add_payload_sample(&data1, app_sensor);
	return rc != -1 ? CMD_OK : CMD_ERROR;
}
*/
	
int mag_get_data(char *fmt, char *params, int nparams)
{
	mag_data_t data1;
	uint32_t mg_msg_len = 128;
	uint32_t time;
	int measurements;
	FILE *mg;
	FILE *py;
	char mag_msg[mg_msg_len];
	char py_msg[mg_msg_len];
	//char path_alt[64] = "/home/pi/suchai-software-template/apps/myapp/src/system/mag.txt";
	char path[] = "sudo python3 /home/pi/suchai-software-template/apps/myapp/src/system/switch.py";
	char number[5];
	int rc;
        
	if (params == NULL)
        return CMD_SYNTAX_ERROR;
    
    if (sscanf(params, fmt, &measurements)!= nparams)
        return CMD_SYNTAX_ERROR;
      
    //LOGI("cmd_mag_get_data", "measurements number %d, time %u", measurements, dat_get_time());
    sprintf(number, " %d", measurements);   
    strcat(path, number);
    //LOGI("cmd_mag_get_data", path);
    
    time = dat_get_time();
    //py = popen(path, "r" );
    if (measurements!= 0)
		system(path);
    
    //while (fgets(py_msg, mg_msg_len, py) != NULL)
    //{
	//}
    //pclose(py);
    
    mg = fopen("/home/pi/mag.txt", "r");
	
	if (mg == NULL)
	{
		LOGI("cmd_mag_get_data", "Null error");
		return CMD_ERROR;
    }
    
    while (fgets(mag_msg, mg_msg_len, mg) != NULL)
    {
		sscanf(mag_msg,"%d %d %d %d %d %d %d %d %f %f", &data1.splf,
														&data1.magxf,
														&data1.magyf,
														&data1.magzf,
														&data1.spls,
														&data1.magxs,
														&data1.magys,
														&data1.magzs,
														&data1.tempf,
														&data1.temps);
    
		data1.index = dat_get_system_var(dat_drp_idx_mag);
		//data1.timestamp = dat_get_time();
		data1.timestamp = time;
		rc = dat_add_payload_sample(&data1, mag_sensor);
	
		//LOGI("cmd_mag_get_data", "indx %u tstp %u splf %d magxf %d magyf %d magzf %d ", data1.index,
		//																				data1.timestamp, 
		//																				data1.splf, 
		//																				data1.magxf, 
		//																				data1.magyf, 
		//																				data1.magzf);
		
		if (rc == -1)
			CMD_ERROR;
		
		time = time+1;
	}
	
    fclose(mg);
	return CMD_OK;
}	
int mag_save_data(char *fmt, char *params, int nparams)
{
	mag_data_t data2;
	
	uint32_t mg_msg_len = 128;
	uint32_t time;

	FILE *mg;

	char mag_msg[mg_msg_len];

	int rc;
        
    time = dat_get_time();
    
    mg = fopen("/home/pi/mag.txt", "r");
	
	if (mg == NULL)
	{
		LOGI("cmd_mag_get_data", "Null error");
		return CMD_ERROR;
    }
    
    while (fgets(mag_msg, mg_msg_len, mg) != NULL)
    {
		sscanf(mag_msg,"%d %d %d %d %d %d %d %d %f %f", &data2.splf,
														&data2.magxf,
														&data2.magyf,
														&data2.magzf,
														&data2.spls,
														&data2.magxs,
														&data2.magys,
														&data2.magzs,
														&data2.tempf,
														&data2.temps);
    
		data2.index = dat_get_system_var(dat_drp_idx_mag);
		//data1.timestamp = dat_get_time();
		data2.timestamp = time;
		rc = dat_add_payload_sample(&data2, mag_sensor);
	
		if (rc == -1)
			LOGI("cmd_mag_get_data", "rc error");
			CMD_ERROR;
		
		time = time+1;
	}
	
    fclose(mg);
	return CMD_OK;
}

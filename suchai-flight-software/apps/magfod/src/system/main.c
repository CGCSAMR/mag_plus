/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *      Copyright 2021, Carlos Gonzalez Cortes, carlgonz@uchile.cl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "suchai/mainFS.h"
#include "suchai/taskInit.h"
#include "suchai/osThread.h"
#include "suchai/log_utils.h"
#include "app/system/taskHousekeeping.h"
#include "app/system/taskFOD.h"
#include "app/system/cmdFOD.h"
#include "app/system/cmdRPI.h"
#include "app/system/cmdHello.h"

#ifdef RPI
#include "csp_if_i2c_uart.h"
#endif

#ifndef SCH_TRX_ADDRESS
#define SCH_TRX_ADDRESS 5   /// TRX NODE ADDRESS
#endif

static char *tag = "app_main";

/**
 * App specific initialization routines
 * This function is called by taskInit
 *
 * @param params taskInit params
 */
void initAppHook(void *params)
{
    /** Include fod app commands */
    cmd_fod_init();

    /** Include RPI commands */
    cmd_rpi_init();

    /** Include MAG commands */
    cmd_hello_init();

    /** Initialize custom CSP interfaces */
#if defined(NANOMIND)
    /* Init csp i2c interface with address 1 and 400 kHz clock */
    LOGI(tag, "Adding CSP I2C INTERFACE...");
    sch_a3200_init_twi0(GS_AVR_I2C_MULTIMASTER, SCH_COMM_NODE, 400000);
    rc = csp_i2c_init(SCH_COMM_NODE, 0, 400000);
    if(rc != CSP_ERR_NONE) LOGE(tag, "\tcsp_i2c_init failed!");

    /**
     * Setting route table
     * Build with options: --enable-if-i2c --with-rtable cidr
     *  csp_rtable_load("8/2 I2C 5");
     *  csp_rtable_load("0/0 I2C");
     */
    csp_rtable_set(8, 2, &csp_if_i2c, SCH_TRX_ADDRESS); // Traffic to GND (8-15) via I2C node TRX
    csp_route_set(CSP_DEFAULT_ROUTE, &csp_if_i2c, CSP_NODE_MAC); // All traffic to I2C using node as i2c address
#elif defined(RPI)
    csp_i2c_uart_init(SCH_COMM_NODE, 0, 19200);
    csp_rtable_set(8, 2, &csp_if_i2c_uart, 5); // Traffic to GND (8-15) via I2C to TRX node
    csp_route_set(CSP_DEFAULT_ROUTE, &csp_if_i2c_uart, CSP_NODE_MAC); // Rest of the traffic to I2C using node i2c address
#elif defined(X86) && defined(LINUX)
    LOGI(tag, "Adding CSP ZMQHUB INTERFACE...");
    csp_add_zmq_iface(SCH_COMM_NODE);
#endif

    /** Init app tasks */
    int t_ok = osCreateTask(taskHousekeeping, "housekeeping", 1024, NULL, 2, NULL);
    if(t_ok != 0) LOGE("fod-app", "Task housekeeping not created!");
    int tfod_ok = osCreateTask(taskFOD, "hello", 1024, NULL, 2, NULL);
    if(tfod_ok != 0) LOGE("fod-app", "Task FOD not created!");
}

int main(void)
{
    /** Call framework main, shouldn't return */
    suchai_main();
}
